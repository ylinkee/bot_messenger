'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var NewSchema = new Schema({
    new_id : {
        type : Number,
        default : 0,
        unique : true
    },
    user_talk : {
        type : [],
        required : true
    },
    bot_reply : {
        type : [],
        required : true
    },
    create_date : {
        type: Date,
        default : Date.now
    },
   status : {
        type: [{
            type: String,
            enum : ['available', 'unavalable']
        }],
        default : ['available']
    }
});
// a setter xử lý trước khi đưa vào database
// FoodSchema.path('name').set((inputString) => {
//     return inputString[0].toUpdateCase()+inputString.slice(1);
// });
autoIncrement.initialize(mongoose.connection);
NewSchema.plugin(autoIncrement.plugin, {
    model: 'NewSchema',
    field: 'new_id',
    startAt: 1,
    incrementBy: 1
});
module.exports = mongoose.model('New', NewSchema);