// # SimpleServer
// A simple chat bot server
var logger = require('morgan');
var path = require('path');
var http = require('http');
var bodyParser = require('body-parser');
var express = require('express');
var mongoose = require('mongoose');
var router = express();
let User = require('./models/userModels');
let News = require('./models/newModels');
var app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
var server = http.createServer(app);
var request = require("request");
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'assets')));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js')); // redirect bootstrap JS
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

// connect mongodb
let options = {
  db : {native_parser : true},
  server : {poolSize: 5},
  user : 'admin',
  pass : 'tanlinh11'
};
// Use native Promises
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://45.32.109.179:27017/vi_bots', options).then(
  () => {
    console.log("Connect mongoDB successfully");
  },
  err => {
    console.log(`Connect failed, Error : ${err}`);
  }
);
// Get index
app.get('/', (req, res) => {
  res.render('index', { title: 'BOT TopQuiz' });
});
app.get('/503', (req, res) => {
  res.render('503', { title: 'BOT TopQuiz' });
});
app.get('/404', (req, res) => {
  res.render('404', { title: 'BOT TopQuiz' });
});
// POST 1 NEW 
app.post('/new',(req,res) => {
  const new_post = new News({
    user_talk : req.body.user.toLowerCase(),
    bot_reply : req.body.bot
  });
  // Ưu tiên kiểm bot_reply trước
  // Nếu đã có trong database rồi thì chỉ việc update push thêm thèn user_talk vào
  // Nếu mới thì quay qua kiểm tra user_talk đã tồn tại chưa rồi làm như dưới
  const reply = {
      bot_reply : req.body.bot
    }
  const question = {
    user_talk : req.body.user.toLowerCase()
  };
  News.find(reply).limit(1).exec((err, newsReply) => {
    if(err){
      res.json({
        result : "Failed newsReply",
        data : [],
        message : `Error : ${err}`
      });
      res.render('404', { title: 'BOT TopQuiz' });
    }else{
      // kiem tra xem bot_reply da ton tai chua
      if(newsReply.length > 0){ // update user_talk co cung bot_reply
        // check tiep bot_reply co ton tai nua ko
        News.find(question).limit(1).exec((err, newSameSame) => {
          if(err){
            res.json({
            result : "Failed newSameSame",
            data : [],
            message : `Error : ${err}`
          });
          res.render('404', { title: 'BOT TopQuiz' });
          }else{
            if(newSameSame.length > 0){ // có ton tai bot_reply nua thi bao la ko up nua
              res.json({
                result : "Failed",
                message : "Đã tồn tại rồi"
              })
            }else{
              News.findOneAndUpdate({bot_reply : req.body.bot},{$push : {user_talk : req.body.user.toLowerCase()}},(err, updateReply) => {
                if(err){
                  res.json({
                    result : "Failed update newsReply",
                    data : [],
                    message : `update error : ${err}`
                  });
                  res.render('503', { title: 'BOT TopQuiz' });
                }else{
                  res.render('index', { title: 'BOT TopQuiz successfully' });
                  res.json({
                    result : "OK",
                    data : updateReply,
                    message : "Update newsReply succsessfully"
                  })
                }
              });
            }
          }
        });
      }else{
          // kiem tra user_talk xem da ton tai chua
        News.find(question).limit(1).exec((err, news) => {
          if(err){
            res.json({
              result : "Failed",
              data : [],
              message : `Error : ${err}`
            });
            res.render('503', { title: 'BOT TopQuiz' });
          }else{
            // kiem tra ton tai question do chua, co thi update lai reply thoi
            if(news.length > 0){ // update
              News.findOneAndUpdate({user_talk : req.body.user.toLowerCase()},{$push : {bot_reply : req.body.bot}},(err, updateNew) => {
                if(err){
                  res.json({
                    result : "Failed",
                    data : [],
                    message : `update error : ${err}`
                  });
                  res.render('503', { title: 'BOT TopQuiz' });
                }else{
                  res.render('index', { title: 'BOT TopQuiz successfully' });
                  res.json({
                    result : "OK",
                    data : updateNew,
                    message : "Update new post succsessfully"
                  });
                }
              });
            }else{ // insert new post
              new_post.save((err) => {
                if(err){
                  res.json({
                    result : "Failed",
                    message : `insert error : ${err}`
                  })
                }else{
                  res.render('index', { title: 'BOT TopQuiz successfully' });
                  res.json({
                    result : "OK",
                    data : news,
                    message : "Insert new post successfully"
                  })
                }
              })
            }
          }
        });
      }
    }
  });

});
//Get news all
app.get('/api/news', function(req,res){
  News.find((err, news) => {
    if(err){
      res.json({
        result : "Failed",
        data : [],
        message : `Get failed, error : ${err}`
      })
    }else{
      res.json({
        result : "OK",
        data : news,
        message : "Get data news successfully!"
      })
    }
  });
});
// Get One news
app.get('/api/news/new_id', function(req,res){
  News.findOne({'user_talk':req.query.q},(err, news) => {
    if(err){
      res.json({
        result : "Failed",
        data : [],
        message : `Get failed, error : ${err}`
      })
    }else{
      res.json({
        result : "OK",
        data : news,
        message : "Get data news successfully!"
      })
    }
  });
});
// Update One bot_reply in news
app.put('/api/news/new_id', function(req,res){
 News.findByIdAndUpdate({"user_talk" : req.query.q},{"bot_reply[0]": req.query.r},(err, updateNew) => {
        if(err){
          res.json({
            result : "Failed",
            data : [],
            message : `update error : ${err}`
          })
        }else{
          res.json({
            result : "OK",
            data : updateNew,
            message : "Update one new succsessfully"
          })
        }
      });
});
// Delete One news with user_talk
app.delete('/api/news/new_id', function(req,res){
  News.findOneAndRemove({'user_talk':req.query.d},(err, news) => {
    if(err){
      res.json({
        result : "Failed",
        data : [],
        message : `Get failed, error : ${err}`
      })
    }else{
      res.json({
        result : "OK",
        data : news,
        message : "Get data news successfully!"
      })
    }
  });
});
// Đây là đoạn code để tạo Webhook
app.get('/webhook', function(req, res) {
  if (req.query['hub.verify_token'] === 'ma_xac_minh_cua_ban') {
    res.send(req.query['hub.challenge']);
  }
  res.send('Error, wrong validation token');
});

// Xử lý khi có người nhắn tin cho bot
app.post('/webhook', function(req, res) {
  var entries = req.body.entry;
  for (var entry of entries) {
    var messaging = entry.messaging;
    for (var message of messaging) {
      var senderId = message.sender.id;
      // user info
      const idUser = senderId;
      User.find({"senderID" : idUser}).limit(1).exec((err, users) => {
        if(err){
          res.json({
            result : "failed",
            data : {},
            message : `Error is : ${err}`
          });
        }else{ 
          // if co ton tai hay chua id do
          if(users.length > 0){ 
            res.json({
            result : "failed",
            data : {},
            message : "Da ton tai"
          });
        }else{ 
            const newUser = new User({
              senderID : idUser
            });
              newUser.save((err) => {
                if(err){
                  res.json({
                    result : "failed",
                    data : {},
                    message : `Error is : ${err}`
                  });
                }else{
                res.json({
                  result : "OK",
                  data: {
                    senderID : idUser,
                    message : "Insert new food successfully"
                  }
                });
              }
            });
          }
        }
      });
      // end user info
      if (message.optin) {
          receivedAuthentication(message);
        } else if (message.message) {
          receivedMessage(message);
        } else if (message.delivery) {
          receivedDeliveryConfirmation(message);
        } else if (message.postback) {
          receivedPostback(message);
        } else if (message.read) {
          receivedMessageRead(message);
        } else if (message.account_linking) {
          receivedAccountLink(message);
        } else {
          console.log("Webhook received unknown messagingEvent: ", message);
        }
    }
  }

  res.status(200).send("OK");
});


// Gửi thông tin tới REST API để trả lời
/*
 * Authorization Event
 *
 * The value for 'optin.ref' is defined in the entry point. For the "Send to 
 * Messenger" plugin, it is the 'data-ref' field. Read more at 
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/authentication
 *
 */
function receivedAuthentication(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfAuth = event.timestamp;

  // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
  // The developer can set this to an arbitrary value to associate the 
  // authentication callback with the 'Send to Messenger' click event. This is
  // a way to do account linking when the user clicks the 'Send to Messenger' 
  // plugin.
  var passThroughParam = event.optin.ref;

  console.log("Received authentication for user %d and page %d with pass " +
    "through param '%s' at %d", senderID, recipientID, passThroughParam, 
    timeOfAuth);

  // When an authentication is received, we'll send a message back to the sender
  // to let them know it was successful.
  sendTextMessage(senderID, "Authentication successful");

}

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message' 
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-received
 *
 * For this example, we're going to echo any text that we get. If we get some 
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've 
 * created. If we receive a message with an attachment (image, video, audio), 
 * then we'll simply confirm that we've received the attachment.
 * 
 */
function receivedMessage(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfMessage = event.timestamp;
  var message = event.message;

  var isEcho = message.is_echo;
  var messageId = message.mid;
  var appId = message.app_id;
  var metadata = message.metadata;
  
  // You may get a text or attachment but not both
  var messageText = message.text;
  var messageAttachments = message.attachments;
  var quickReply = message.quick_reply;

  if (isEcho) {
    // Just logging message echoes to console
    console.log("Received echo for message %s and app %d with metadata %s", 
      messageId, appId, metadata);
    return;
  } else if (quickReply) {
    var quickReplyPayload = quickReply.payload;
    console.log("Quick reply for message %s with payload %s",
      messageId, quickReplyPayload);
      // nhận quickReply - phân loại và xử lý
      if(quickReplyPayload == 'Iq'){
        sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
        setTimeout(function(){
          sendQuizIqMessage(senderID);
        },1000);
      }else if(quickReplyPayload == 'suy'){
        sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
        setTimeout(function(){
          sendQuizSuyluanMessage(senderID);
        },1000);
      }else if(quickReplyPayload == 'biệt'){
        sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
        setTimeout(function(){
          sendQuizKhacbietMessage(senderID);
        },1000);
      }else if(quickReplyPayload == 'đạo'){
        sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
        setTimeout(function(){
          sendCunghoangdaoMessage(senderID);
        },1000);
      }else if(quickReplyPayload == 'hot'){
        sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
        setTimeout(function(){
          sendQuizMessage(senderID);
        },1000);
      }else if(quickReplyPayload == 'game'){
        sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
        setTimeout(function(){
          sendQuizGameMessage(senderID);
        },1000);
      }else if(quickReplyPayload == 'khác'){
        sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
        setTimeout(function(){
          sendQuizRandomMessage(senderID);
        },1000);
      }else if(quickReplyPayload == 'mới'){
        sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
        setTimeout(function(){
          sendQuizNewsMessage(senderID);
        },1000);
      }else{
        sendTextMessage(senderID, "Không nhận được message");
      }
    return;
  }else if(messageAttachments){
    // user gởi lên image
    sendTextMessage(senderID," :D ");
    setTimeout(function(){
      sendTextMessage(senderID," Bạn gởi cái gì vậy ? Chơi Quiz không ?");
    },1000);
    setTimeout(function(){
      sendButtonMessage(senderID);
    },2000);
      return;
  }

  if (messageText) {  
      // Event Cung hoàng đạo điểm yếu
      if(messageText.split("",1).some(function(d){return d ==='0' || d === '1' || d === '2' || d === '3'}) == true){
        var arr = messageText.split(""); 
        var day = arr[0] + arr[1]; 
        var mon = arr[2] + arr[3]; 
        var year = arr[4] + arr[5] + arr[6] + arr[7]; 
        if(21 <= day && day <= 31 && mon == 03 || 01 <= day && day <= 19 && mon == 04 ){
          // Cung Bạch Dương
          sendTextMessage(senderID," Bạn thuộc cung Bạch Dương");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Rất tham vọng nên không mấy khi được tận hưởng cảm giác bình yên. Khó yêu nhưng một khi đã yêu thì rất mãnh liệt và dễ bị tình cảm chi phối. Thất thường, dễ nóng nảy, bốc đồng, tự cho bản thân là số 1");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(20 <= day && day <= 30 && mon == 04 || 01 <= day && day <= 20 && mon == 05 ){
          // Cung Kim ngưu
          sendTextMessage(senderID," Bạn thuộc cung Kim Ngưu");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Ương ngạnh và bướng bỉnh trong tình cảm. Lười biếng, ích kỷ, thường coi trọng vật chất");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(21 <= day && day <= 31 && mon == 05 || 01 <= day && day <= 21 && mon == 06 ){
          // Cung Song Tử
          sendTextMessage(senderID," Bạn thuộc cung Song Tử");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Hay bồn chồn, lo lắng. Hời hợt, bốc đồng. Không ngay thẳng");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(22 <= day && day <= 30 && mon == 06 || 01 <= day && day <= 22 && mon == 07 ){
          // Cung Cự Giải
          sendTextMessage(senderID," Bạn thuộc cung Cự Giải");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Tính khí thất thường, khó kiềm chế khi nóng giận. Quá nhạy cảm, dễ bị tổn thương. Thích dựa dẫm, hay suy nghĩ lung tung");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(23 <= day && day <= 31 && mon == 07 || 01 <= day && day <= 22 && mon == 08 ){
          // Cung Sư Tử
          sendTextMessage(senderID," Bạn thuộc cung Sư Tử");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Dễ mềm lòng với lời nói ngon ngọt, khen ngợi. Độc đoán, khoa trương, cứng đầu, kiêu căng");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(23 <= day && day <= 31 && mon == 08 || 01 <= day && day <= 22 && mon == 09 ){
          // Cung Xử Nữ
          sendTextMessage(senderID," Bạn thuộc cung Xử Nữ");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Hay nghi ngờ, quá kiểu cách. Hay rụt rè trước người lạ. Quan trọng hóa vấn đề. Cực đoan, dễ bị tổn thương trong tình cảm");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(23 <= day && day <= 30 && mon == 09 || 01 <= day && day <= 23 && mon == 10 ){
          // Cung Thiên Bình
          sendTextMessage(senderID," Bạn thuộc cung Thiên Bình");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Hay đắn đo, không dứt khoát. Hay mơ mộng hão huyền. Làm gì cũng sợ sai");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(24 <= day && day <= 31 && mon == 10 || 01 <= day && day <= 22 && mon == 11 ){
          // Cung Thần Nông
          sendTextMessage(senderID," Bạn thuộc cung Thần Nông");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Ghen tuông, hay hoài nghi. Dễ gây ám ảnh, dễ ức chế. Ít nói và che giấu cảm xúc khiến người khác thấy xa cách");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(23 <= day && day <= 30 && mon == 11 || 01 <= day && day <= 21 && mon == 12 ){
          // Cung Nhân Mã
          sendTextMessage(senderID," Bạn thuộc cung Nhân Mã");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Ham chơi, nói nhiều. Hay dối lòng. Hay vô tình làm tổn thương người khác");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(22 <= day && day <= 31 && mon == 12 || 01 <= day && day <= 19 && mon == 01 ){
          // Cung Ma Kết
          sendTextMessage(senderID," Bạn thuộc cung Ma Kết");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Tự phụ về bản thân. Hay để mất cơ hội tốt. Quá nghiêm túc, dễ bị ức chế");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(20 <= day && day <= 31 && mon == 01 || 01 <= day && day <= 18 && mon == 02 ){
          // Cung Bảo Bình
          sendTextMessage(senderID," Bạn thuộc cung Bảo Bình");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Lạnh lùng, vô cảm, cứng đầu. Hay mỉa mai, hay ảo tưởng. Không thích coi trọng quy tắc");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else if(19 <= day && day <= 28 && mon == 02 || 01 <= day && day <= 20 && mon == 03 ){
          // Cung Song Ngư
          sendTextMessage(senderID," Bạn thuộc cung Song Ngư");
          setTimeout(function(){
            sendTextMessage(senderID,"Điểm yếu của bạn là : Thất thường, nhạy cảm quá mức. Trốn tránh thực tế. Lười biếng, hay lưỡng lự");
          },1000);
          setTimeout(function(){
            sendTextMessage(senderID," Khám phá điểm mạnh của bạn ở đây nhé <3");
          },3000);
          setTimeout(function(){
            sendCunghoangdaoMessage(senderID);
          },4000);
        }else{
          // Không đúng địng dạng
          sendTextMessage(senderID," Nhập ngày tháng năm sinh chưa đúng nha! Ví dụ : 05101993 hoặc 0510");
        }
      }else if(messageText.split("",1).some(function(d){return d ==='4' || d === '5' || d === '6' || d === '7' || d === '8' || d === '9'}) == true){
          sendTextMessage(senderID," Nhập ngày tháng năm sinh chưa đúng nha! Ví dụ : 05101993 hoặc 0510");
      }else 
      // end cung hoàng đạo
      // If we receive a text message, check to see if it matches any special
      // keywords and send back the corresponding example. Otherwise, just echo
      // the text we received.
      if(messageText.split(" ").some(function(w){return (w === 'Quiz' || w === 'quiz' || w === 'topquiz' || w === 'Topquiz' || w === 'TOPQUIZ')})==true && messageText.split(" ").some(function(w){return (w === 'là' || w === 'la')})==true && messageText.split(" ").some(function(w){return (w === 'gì' || w === 'gi')})==true){
          sendTextMessage(senderID," Chơi thử là biết nè! ");
          setTimeout(function(){
            sendQuizMessage(senderID);
          },1000);
      }else if(messageText.split(" ").some(function(w){return w === 'Giúp' || w === 'giúp' || w === 'giup' || w === 'Giup' || w === 'Cho' || w === 'cho'})==true && messageText.split(" ").some(function(w){return w === 'Mình' || w === 'mình' || w === 'tôi' || w === 'Tôi' || w ==='minh'})==true && messageText.split(" ").some(function(w){return w === 'với' || w === 'voi' || w === 'hỏi' || w === 'hoi' || w ==='Hỏi'})==true){
          sendTextMessage(senderID," Trong thời gian chờ đợi câu trả lời, hãy chơi thử cái này xem sao nhé, biết đâu lại thông minh ra ");
          setTimeout(function(){
            sendQuizIqMessage(senderID);
          },1000);
      }else if(messageText.split(" ").some(function(w){return w === 'IQ' || w === 'Iq' || w === 'iq'})==true && messageText.split(" ").some(function(w){return w === 'Đúng' || w === 'đúng' || w === 'dung' || w === 'đung' || w ==='Dung'})==true && messageText.split(" ").some(function(w){return w === 'Không' || w === 'không' || w === 'khong' || w === 'ko' || w ==='Ko'})==true){
          sendTextMessage(senderID," Chuẩn không cần chỉnh, chỉnh là hết chuẩn nhá =))");
          setTimeout(function(){
            sendTextMessage(senderID, " Chơi lại thử xem có đúng không nhé!");
          },1000);
          setTimeout(function(){
            sendQuizIqMessage(senderID);
          },2000);
      }else if(messageText.split(" ").some(function(w){return w === 'Iq' || w === 'iq' || w === 'quiziq' || w === 'IQ'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendQuizIqMessage(senderID);
          },1000);
      }else if(messageText.split(" ").some(function(w){return w === 'Game' || w === 'game'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendQuizGameMessage(senderID);
          },1000);
      }else if(messageText.split(" ").some(function(w){return w === 'Bạo' || w === 'bạo' || w === 'FBI' || w === 'Fbi' || w === 'CIA' || w === 'Cia' || w === 'fpi'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendQuizFbiMessage(senderID);
          },1000);
      }else if(messageText.split(" ").some(function(w){return w === 'Đố' || w === 'đố' || w === 'dố' || w === 'thách' || w === 'Thách'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendQuizDovuiMessage(senderID);
          },1000);
      }else if(messageText.split(" ").some(function(w){return w === 'khác' || w === 'khac' || w === 'Khác'})==true && messageText.split(" ").some(function(w){return w === 'biệt' || w === 'biet'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendQuizKhacbietMessage(senderID);
          },1000);          
      }else if(messageText.split(" ").some(function(w){return w === 'Suy' || w === 'suy'})==true && messageText.split(" ").some(function(w){return w === 'luận' || w === 'luan'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendQuizSuyluanMessage(senderID);
          },1000);          
      }else if(messageText.split(" ").some(function(w){return w === 'quiz' || w === 'Quiz'})==true && messageText.split(" ").some(function(w){return w === 'mới' || w === 'moi'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendQuizNewsMessage(senderID);
          },1000);          
      }else if(messageText.split(" ").some(function(w){return w === 'mới' || w === 'Mới' || w === 'news' || w === 'moi' || w === 'MỚI'})==true && messageText.split(" ").some(function(w){return w === 'nhất' || w === 'nhat' || w === 'Nhất'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendQuizNewsMessage(senderID);
          },1000);          
      }else if(messageText.split(" ").some(function(w){return w === 'quiz' || w === 'Quiz'})==true && messageText.split(" ").some(function(w){return w === 'khác' || w === 'Khác' || w === 'gì'})==true){
          sendTextMessage(senderID," OK! Có ngay cho sếp !");
          setTimeout(function() {
              sendQuizRandomMessage(senderID);
          },1000);
      }else if(messageText.split(" ").some(function(w){return w === 'Random' || w === 'random' || w === 'ramdom'})==true){
          sendTextMessage(senderID," OK! Có ngay cho sếp !");
          setTimeout(function() {
              sendQuizRandomMessage(senderID);
          },1000);
      }else if(messageText.split(" ").some(function(w){return w === 'chơi' || w === 'Chơi' || w === 'Play' || w === 'play'})==true && messageText.split(" ").some(function(w){return w === 'Quiz' || w === 'quiz' || w === 'quizz'})==true){
          sendTextMessage(senderID," Đợi xíu có ngay cho sếp !");
          setTimeout(function(){
            sendButtonMessage(senderID);
          },1000);          
      }else{
        // Get data from database
        request({
          url: "https://vi.chatbot.ylinkee.com/api/news"
          , method: "get"
          , timeout: 5000
          , json: true
        }, function (err, rs) {
          if (err) {
            console.log('Loi ket noi get data', err);
          }
          else { 
            if (!!rs.body && !!rs.body.data) {
              loop1:
              for(var i = 0;i < rs.body.data.length; i++){
                loop2:
                for(var j = 0; j < rs.body.data[i].user_talk.length; j++){
                  if(messageText.toLowerCase() == rs.body.data[i].user_talk[j]){ // kiem tra xem messageText co = user_talk
                    var ran = Math.floor(Math.random()*rs.body.data[i].bot_reply.length);
                    sendTextMessage(senderID,rs.body.data[i].bot_reply[ran]);
                    break loop1;
                  }
                }
                // Neu khong co trong data thi cho BOT noi Khong hieu
                if(i==rs.body.data.length){
                  var ran = Math.floor(Math.random()*4);
                  if(ran == 0){
                    sendTextMessage(senderID," BOT chưa hiểu ý bạn nói, bạn thông cảm nhé !");
                  }else{
                    sendTextMessage(senderID,messageText);
                  }
                } 
              }
            }
          }
        });  
      }
  }
}
/*
 * Delivery Confirmation Event
 *
 * This event is sent to confirm the delivery of a message. Read more about 
 * these fields at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-delivered
 *
 */
function receivedDeliveryConfirmation(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var delivery = event.delivery;
  var messageIDs = delivery.mids;
  var watermark = delivery.watermark;
  var sequenceNumber = delivery.seq;

  if (messageIDs) {
    messageIDs.forEach(function(messageID) {
      console.log("Received delivery confirmation for message ID: %s", 
        messageID);
    });
  }
  console.log("All message before %d were delivered.", watermark);
  }
/*
 * Postback Event
 *
 * This event is called when a postback is tapped on a Structured Message. 
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/postback-received
 * 
 */
function receivedPostback(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfPostback = event.timestamp;

  // The 'payload' param is a developer-defined field which is set in a postback 
  // button for Structured Messages. 
  var payload = event.postback.payload;

  console.log("Received postback for user %d and page %d with payload '%s' " + 
    "at %d", senderID, recipientID, payload, timeOfPostback);

  // When a postback is called, we'll send a message back to the sender to 
  // let them know it was successful
  sendTextMessage(senderID, "Vậy bạn có cần mình giúp đỡ gì không ?");
}

/*
 * Message Read Event
 *
 * This event is called when a previously-sent message has been read.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-read
 * 
 */
function receivedMessageRead(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timestamp = event.timestamp;
  // All messages before watermark (a timestamp) or sequence have been seen.
  var watermark = event.read.watermark;
  var sequenceNumber = event.read.seq;
}

/*
 * Account Link Event
 *
 * This event is called when the Link Account or UnLink Account action has been
 * tapped.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/account-linking
 * 
 */
function receivedAccountLink(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;

  var status = event.account_linking.status;
  var authCode = event.account_linking.authorization_code;

  console.log("Received account link event with for user %d with status %s " +
    "and auth code %s ", senderID, status, authCode);
}
// send API quiz mới nhất
function sendQuizNewsMessage(recipientId){
      // Gọi API
  request({
				url: "http://vi.topquiz.co/view/loadmore/1"
				, method: "get"
				, timeout: 5000
				, json: true
			}, function (err, rs) {
				if (err) {
					console.log('Loi ket noi get data', err);
				}
				else {
					// if (callback) callback(rs.body);
          if (!!rs.body && !!rs.body.data) {
						var messageData = {
                recipient: {
                  id: recipientId
                },
                message: {
                  attachment: {
                    type: "template",
                    payload: {
                      template_type: "generic",
                      elements: [{
                        title: rs.body.data[0].title,
                        subtitle: rs.body.data[0].description,
                        item_url: rs.body.data[0].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[0].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[0].pid + '?ref=mes',              
                        image_url: rs.body.data[0].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body.data[0].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[0].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[0].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      },{
                        title: rs.body.data[1].title,
                        subtitle: rs.body.data[1].description,
                        item_url: rs.body.data[1].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[1].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[1].pid + '?ref=mes',              
                        image_url: rs.body.data[1].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body.data[1].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[1].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[1].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      },{
                        title: rs.body.data[2].title,
                        subtitle: rs.body.data[2].description,
                        item_url: rs.body.data[2].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[2].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[2].pid + '?ref=mes',              
                        image_url: rs.body.data[2].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body.data[2].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[2].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[2].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      },{
                        title: rs.body.data[3].title,
                        subtitle: rs.body.data[3].description,
                        item_url: rs.body.data[3].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[3].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[3].pid + '?ref=mes',              
                        image_url: rs.body.data[3].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body.data[3].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[3].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[3].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      },{
                        title: rs.body.data[4].title,
                        subtitle: rs.body.data[4].description,
                        item_url: rs.body.data[4].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[4].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[4].pid + '?ref=mes',              
                        image_url: rs.body.data[4].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body.data[4].type =1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body.data[4].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body.data[4].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      }]
                    }
                  }
                }
              };
              callSendAPI(messageData);
					}
				}
			});    
}
// send Quiz random
function sendQuizRandomMessage(recipientId){
      // Gọi API
  request({
				url: "http://vi.topquiz.co/view/recommend/q/1"
				, method: "get"
				, timeout: 10000
				, json: true
			}, function (err, rs) {
				if (err) {
					console.log('Loi ket noi get data', err);
				}
				else { 
          if (!!rs.body) {
						var messageData = {
                recipient: {
                  id: recipientId
                },
                message: {
                  attachment: {
                    type: "template",
                    payload: {
                      template_type: "generic",
                      elements: [{
                        title: rs.body[0].title,
                        subtitle: rs.body[0].description,
                        item_url: rs.body[0].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[0].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[0].pid + '?ref=mes',              
                        image_url: rs.body[0].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body[0].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[0].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[0].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      },{
                        title: rs.body[1].title,
                        subtitle: rs.body[1].description,
                        item_url: rs.body[1].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[1].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[1].pid + '?ref=mes',              
                        image_url: rs.body[1].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body[1].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[1].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[1].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      },{
                        title: rs.body[2].title,
                        subtitle: rs.body[2].description,
                        item_url: rs.body[2].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[2].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[2].pid + '?ref=mes',              
                        image_url: rs.body[2].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body[2].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[2].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[2].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      },{
                        title: rs.body[3].title,
                        subtitle: rs.body[3].description,
                        item_url: rs.body[3].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[3].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[3].pid + '?ref=mes',              
                        image_url: rs.body[3].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body[3].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[3].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[3].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      },{
                        title: rs.body[4].title,
                        subtitle: rs.body[4].description,
                        item_url: rs.body[4].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[4].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[4].pid + '?ref=mes',              
                        image_url: rs.body[4].thumb.medium,
                        buttons: [{
                          type: "web_url",
                          url: rs.body[4].type ==1 ? 'http://vi.topquiz.co/quiz/iq/' + rs.body[4].pid + '?ref=mes' : 'http://vi.topquiz.co/quiz/' + rs.body[4].pid + '?ref=mes',
                          title: "Chơi Ngay"
                        }]
                      }]
                    }
                  }
                }
              };
              callSendAPI(messageData);
					}else{console.log("Không get được data");}
				}
			});    
}

// send quiz Bạo lực
function sendQuizFbiMessage(recipientId){
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "6 câu hỏi 'xoắn não' phải vượt qua để gia nhập FBI",
            subtitle: "Khả năng suy luận và quan sát thật tốt mới mong vượt qua bài test này",
            item_url: "http://vi.topquiz.co/quiz/352?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/59802ef9ee0226140ab25d9b/thumb_720/thumb.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/352?ref=mes",
              title: "Chơi Ngay"
            }]
          }, {
            title: "Bạn có tự tin mình đủ tố chất trở thành đặc vụ FBI?",
            subtitle: "Bạn có tin mình phù hợp với công việc mà chỉ có 3,5% dân số thế giới dám thử?",
            item_url: "http://vi.topquiz.co/quiz/309?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/5948cac440815e08eb82714f/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/309?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: "Bạn có đủ tố chất để trở thành thám tử hay chưa?",
            subtitle: "Bài test thám tử chỉ có hơn 7% dân số thế giới có thể vượt qua, bạn có dám một lần thử sức?",
            item_url: "http://vi.topquiz.co/quiz/iq/345?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/59796ebc8b3574139865ee53/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/345?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Trực giác của bạn mạnh mẽ đến đâu?',
            subtitle: 'Khám phá giác quan thứ 6 trong bạn',
            item_url: "http://vi.topquiz.co/quiz/iq/320?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/59530fde9b4ca8169d4387b6/thumb_720/THUMB.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/320?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Bạn có vượt qua bài test để trở thành CIA không?',
            subtitle: 'Chỉ người có óc suy luận tốt mới vượt qua kì thi này',
            item_url: "http://vi.topquiz.co/quiz/288?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/592bcffe53f3ee3026ae290a/thumb_720/THUMB.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/288?ref=mes",
              title: "Chơi Ngay"
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}
// send quiz Đố vui
function sendQuizDovuiMessage(recipientId){
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "Vòng tròn nào nằm trên - Đố bạn biết!",
            subtitle: "Ngon thì vào chơi đi, khó quá không chơi được thì đừng buồn nhé!",
            item_url: "http://vi.topquiz.co/quiz/1?ref=mes",               
            image_url:"http://vi.topquiz.co/public/images/uploads/thumb/quiz_1479722088000/large_quiz_1479722088000.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/1?ref=mes",
              title: "Chơi Ngay"
            }]
          }, {
            title: "9/10 người chơi đầu hàng trước bài test thị giác này!",
            subtitle: "Đa số người chơi đều vượt qua được 5 câu đầu, nhưng đến câu cuối thì chỉ còn 10% bám trụ được, bạn thì sao?",
            item_url: "http://vi.topquiz.co/quiz/iq/347?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/597edf6960e83225c040723d/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/347?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: "7 câu đố trẻ em khiến người thông minh cũng phải đau đầu",
            subtitle: "Tưởng không khó mà khó không tưởng",
            item_url: "http://vi.topquiz.co/quiz/iq/329?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/59643261b1ab9a596362943e/thumb_720/00.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/329?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Bài kiểm tra trí tuệ 90% người chơi không thể vượt qua',
            subtitle: 'Bạn có dám tự tin là mình sẽ vượt qua 90% dân số thế giới để hoàn thành bài kiểm tra này một cách chính xác?',
            item_url: "http://vi.topquiz.co/quiz/iq/319?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/595229369b4ca8169d438793/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/319?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Kiểm tra trí thông minh bằng bài toán cấp 1 siêu đơn giản',
            subtitle: 'Không vượt qua nổi bài kiểm tra này thì về học lại cấp 1 đi nhé!',
            item_url: "http://vi.topquiz.co/quiz/iq/348?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/597ff93364f4ac134e14b11c/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/348?ref=mes",
              title: "Chơi Ngay"
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}
// send quiz Game
function sendQuizGameMessage(recipientId){
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "GAME: Ai thuộc bảng chữ cái Alphabet giỏi nhất?",
            subtitle: "Đố ai nhớ chính xác mà không lật sách vở ra kiểm tra đấy!",
            item_url: "http://vi.topquiz.co/quiz/149?ref=mes",               
            image_url:"http://vi.topquiz.co/public/images/uploads/thumb/quiz_1478136144000/large_quiz_1478136144000.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/149?ref=mes",
              title: "Chơi Ngay"
            }]
          }, {
            title: "Phá đảo game màu sắc ảo tung chảo",
            subtitle: "Những người sở hữu cặp mắt tinh tường ở đâu rồi? Xuất hiện đi nào!",
            item_url: "http://vi.topquiz.co/quiz/242?ref=mes",               
            image_url:"http://vi.topquiz.co/public/images/uploads/thumb/quiz_1490684393000/large_quiz_1490684393000.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/242?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: "Kiểm tra sự nhanh nhạy bằng loạt câu hỏi 10 giây",
            subtitle: "Mỗi câu hỏi chỉ có 10 giây thôi, hãy suy nghĩ thật nhanh",
            item_url: "http://vi.topquiz.co/quiz/iq/304?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/5940a6ee394a0f67173e1354/thumb_720/00.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/304?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Bạn có đủ tỉnh táo để thoát khỏi bẫy trong loạt hình sau',
            subtitle: 'Những hình ảnh ảo giác khiến phần lớn đau mắt mệt não',
            item_url: "http://vi.topquiz.co/quiz/iq/357?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/598934d095534154e97c7dff/thumb_720/0.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/357?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'IQ màu sắc của bạn đang ở mức nào?',
            subtitle: 'Thách thức khả năng nhận biết và pha trộn màu sắc của bạn.',
            item_url: "http://vi.topquiz.co/quiz/iq/274?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/5923b6ad6c1d991727297138/thumb_720/00.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/274?ref=mes",
              title: "Chơi Ngay"
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}
// send API quiz IQ
function sendQuizIqMessage(recipientId){
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "Chấm điểm IQ qua 7 bức ảnh đánh lừa não bộ",
            subtitle: "Liệu tư duy bằng hình ảnh có phải là thế mạnh của bạn?",
            item_url: "http://vi.topquiz.co/quiz/iq/356?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/59881b76f25d4f4e0f7f358d/thumb_720/01.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/356?ref=mes",
              title: "Chơi Ngay"
            }]
          }, {
            title: "Test chỉ số IQ mới nhất năm 2017",
            subtitle: "Đo chỉ số thông minh của bạn đi nào",
            item_url: "http://vi.topquiz.co/quiz/iq/332?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/596c265bebd38a4b71f1c9c7/thumb_720/00.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/332?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: "Bài test IQ dự đoán khả năng thành công tương lai của bạn ",
            subtitle: "Chỉ số IQ của bạn là bao nhiêu?",
            item_url: "http://vi.topquiz.co/quiz/iq/326?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/595df76a9b4ca8169d438852/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/326?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Kiểm tra chỉ số IQ siêu chính xác',
            subtitle: '6/10 người chơi không thể vượt qua bài kiểm tra này, bạn dám thử hay không?',
            item_url: "http://vi.topquiz.co/quiz/iq/293?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/5934b65e6e2cb2395995224a/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/293?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'IQ màu sắc của bạn đang ở mức nào?',
            subtitle: 'Thách thức khả năng nhận biết và pha trộn màu sắc của bạn.',
            item_url: "http://vi.topquiz.co/quiz/iq/274?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/5923b6ad6c1d991727297138/thumb_720/00.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/274?ref=mes",
              title: "Chơi Ngay"
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}
// send API quiz Điểm khác biệt
function sendQuizKhacbietMessage(recipientId){
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "Tìm hình khác biệt - thách thức đôi mắt tinh anh",
            subtitle: "Luyện 'cơ mắt' với 8 hình ảnh siêu khó ",
            item_url: "http://vi.topquiz.co/quiz/iq/346?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/597eda9c60e83225c0407218/thumb_720/00.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/346?ref=mes",
              title: "Chơi Ngay"
            }]
          }, {
            title: "Bài test thị giác 90% dân số phải bó tay",
            subtitle: "Kiểm tra thị giác của bạn thông qua thử thách tìm điểm khác biệt, bạn dám thử hay không?",
            item_url: "http://vi.topquiz.co/quiz/iq/333?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/596edf466f95416457283f74/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/333?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: "Thách thức tìm điểm khác biệt - Bạn dám thử hay không? ",
            subtitle: "Liệu bạn có thể tìm ra điểm khác biệt trong 7 bức hình mà 90% dân số thế giới phải bó tay?",
            item_url: "http://vi.topquiz.co/quiz/iq/312?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/594a237240815e08eb8271a6/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/312?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Thử thách sự tỉ mỉ thông qua 10 hình ảnh siêu rối',
            subtitle: 'Muốn biết mình có tỉ mỉ không, hãy tìm hình khác biệt trong các hình sau',
            item_url: "http://vi.topquiz.co/quiz/iq/278?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/5923eea0e88b85198ea9a204/thumb_720/thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/278?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Chỉ người có con mắt thứ 3 mới có thể vượt qua loạt câu hỏi này',
            subtitle: 'Hãy hít thật sâu trước khi chơi vì bạn chỉ có 10s thôi.',
            item_url: "http://vi.topquiz.co/quiz/iq/323?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/595af5e39b4ca8169d4387eb/thumb_720/thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/323?ref=mes",
              title: "Chơi Ngay"
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}
// send API quiz suy luận
function sendQuizSuyluanMessage(recipientId){
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: '6 câu hỏi "xoắn não" phải vượt qua để gia nhập FBI',
            subtitle: "Khả năng suy luận và quan sát thật tốt mới mong vượt qua bài test này",
            item_url: "http://vi.topquiz.co/quiz/352?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/59802ef9ee0226140ab25d9b/thumb_720/thumb.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/352?ref=mes",
              title: "Chơi Ngay"
            }]
          }, {
            title: "Kiểm tra độ nhạy bén trong suy luận của bạn",
            subtitle: "Trả lời được 5/6 thì bạn chắc chắn là một thiên tài",
            item_url: "http://vi.topquiz.co/quiz/iq/344?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/597814a5eb23f21020b46fc5/thumb_720/Untitled-2.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/344?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: "Khả năng suy luận của bạn có tốt hơn cậu bé Conan?",
            subtitle: "Hãy coi mình là nhà thám tử và phá các vụ án sau",
            item_url: "http://vi.topquiz.co/quiz/334?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/596f0ffbe7bd3066a3eb26d8/thumb_720/00.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/334?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Bạn có vượt qua bài test để trở thành CIA không?',
            subtitle: 'Muốn biết mình có tỉ mỉ không, hãy tìm hình khác biệt trong các hình sau',
            item_url: "http://vi.topquiz.co/quiz/288?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/592bcffe53f3ee3026ae290a/thumb_720/THUMB.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/288?ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Bạn có phải là thiên tài suy luận?',
            subtitle: '87% dân số thế giới không thể hoàn thành bài kiểm tra suy luận này với số điểm tối đa. Bạn thì sao?',
            item_url: "http://vi.topquiz.co/quiz/iq/301?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/593e5b7e020dfa4fbf08e6b6/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/301?ref=mes",
              title: "Chơi Ngay"
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}
// send API cung hoàng đạo
function sendCunghoangdaoMessage(recipientId){
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "Ưu điểm tuyệt vời nhất của 12 cung hoàng đạo là gì?",
            subtitle: "Khám phá ưu điểm bản thân qua cung hoàng đạo nhé!",
            item_url: "http://vi.topquiz.co/quiz/99?utm_source=message&ref=mes",               
            image_url:"http://vi.topquiz.co/public/images/uploads/thumb/quiz_1470906602000/large_quiz_1470906602000.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/99?utm_source=message&ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: "Cung hoàng đạo nào sẽ mang lại may mắn cho bạn?",
            subtitle: "Tìm cho mình một cung hoàng đạo hợp cạ để mang may mắn đến cho nhau nào!",
            item_url: "http://vi.topquiz.co/quiz/34?utm_source=message&ref=mes",               
            image_url:"http://vi.topquiz.co/public/images/uploads/thumb/quiz_1471916713000/large_quiz_1471916713000.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/34?utm_source=message&ref=mes",
              title: "Chơi Ngay"
            }]
          },{
            title: 'Nếu sống trong Hoàng Cung, bạn sẽ là ai?',
            subtitle: 'Lỡ một ngày như thế thật thì sao nhỉ?',
            item_url: "http://vi.topquiz.co/quiz/81?utm_source=message&ref=mes",               
            image_url:"http://vi.topquiz.co/public/images/uploads/thumb/quiz_1474528422000/large_quiz_1474528422000.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/81?utm_source=message&ref=mes",
              title: "Chơi Ngay"
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}
/*
 * Send an image using the Send API.
 *
 */
function sendImageMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "image",
        payload: {
          url: "https://giphy.com/gifs/dog-shiba-inu-typing-mCRJDo24UvJMA"
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send an image using the Send API.
 *
 */
function sendHelloMessage(recipientId, messageText) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: "Chào bạn! Mình có thể giúp gì cho bạn?",
      metadata: "Chào bạn! Mình có thể giúp gì cho bạn?"
    }
  };
  
  callSendAPI(messageData);
}

/*
 * Send an quiz using the Send API.
 *
 */
function sendQuizMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "Thách thức tìm điểm khác biệt - Bạn dám thử hay không?",
            subtitle: "Liệu bạn có thể tìm ra điểm khác biệt trong 7 bức hình mà 90% dân số thế giới phải bó tay?",
            item_url: "http://vi.topquiz.co/quiz/iq/312?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/594a237240815e08eb8271a6/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/312?ref=mes",
              title: "Chơi Ngay"
            }, {
              type: "postback",
              title: "Để Sau",
              payload: "Bạn có muốn chơi quiz khác không ?",
            }],
          }, {
            title: "Trực giác của bạn mạnh mẽ đến đâu?",
            subtitle: "Khám phá giác quan thứ 6 trong bạn",
            item_url: "http://vi.topquiz.co/quiz/iq/320?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/59530fde9b4ca8169d4387b6/thumb_720/THUMB.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/320?ref=mes",
              title: "Chơi Ngay"
            }, {
              type: "postback",
              title: "Để sau",
              payload: "Bạn có muốn chơi quiz khác không ?",
            }]
          },{
            title: "Đo mắt với loạt hình ảnh siêu khó",
            subtitle: "Không chỉ tinh mắt mà còn phải có trí tưởng tượng bay xa bay cao nữa mới đạt điểm tối đa đấy nhé!",
            item_url: "http://vi.topquiz.co/quiz/iq/279?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/5924fcebe88c7f1aa8dd50f8/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/279?ref=mes",
              title: "Chơi Ngay"
            }, {
              type: "postback",
              title: "Để Sau",
              payload: "Bạn có muốn chơi quiz khác không ?",
            }],
          },{
            title: 'Đo độ "tỉnh" với 9 hình ảnh gây ảo giác',
            subtitle: 'Chỉ có thánh "tỉnh" mới không bị lừa',
            item_url: "http://vi.topquiz.co/quiz/216?ref=mes",               
            image_url:"http://vi.topquiz.co/public/images/uploads/thumb/quiz_1486346927000/large_quiz_1486346927000.jpeg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/216?ref=mes",
              title: "Chơi Ngay"
            }, {
              type: "postback",
              title: "Để Sau",
              payload: "Bạn có muốn chơi quiz khác không ?",
            }],
          },{
            title: "Thiên tài sử dụng trên 10% não bộ để trả lời 9 câu này",
            subtitle: "Kiểm tra xem mình có tố chất của một thiên tài không nào!",
            item_url: "http://vi.topquiz.co/quiz/iq/290?ref=mes",               
            image_url:"http://vi.topquiz.co/images/quizz/592cee0bb92ede305f0067ae/thumb_720/Thumb.jpg",
            buttons: [{
              type: "web_url",
              url: "http://vi.topquiz.co/quiz/iq/290?ref=mes",
              title: "Chơi Ngay"
            }, {
              type: "postback",
              title: "Để Sau",
              payload: "Bạn có muốn chơi quiz khác không ?",
            }],
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}
/*
 * Send a Gif using the Send API.
 *
 */
function sendGifMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "image",
        payload: {
          url: "https://giphy.com/gifs/dog-shiba-inu-typing-mCRJDo24UvJMA"
        }
      }
    }
  };

  callSendAPI(messageData);
}
/*
 * Send a text message using the Send API.
 *
 */
function sendTextMessage(recipientId, messageText) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: messageText,
      metadata: "DEVELOPER_DEFINED_METADATA"
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a button message using the Send API.
 *
 */
function sendButtonMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: "Lựa chọn định dạng quiz mà bạn muốn chơi ^^",
      quick_replies: [
        {
          "content_type":"text",
          "title":"Quiz Iq",
          "payload":"Iq"
        },
        {
          "content_type":"text",
          "title":"Game",
          "payload":"game"
        },
        {
          "content_type":"text",
          "title":"Quiz suy luận",
          "payload":"suy"
        },
        {
          "content_type":"text",
          "title":"Quiz tìm điểm khác biệt",
          "payload":"biệt"
        },
        {
          "content_type":"text",
          "title":"Quiz cung hoàng đạo",
          "payload":"đạo"
        },
        {
          "content_type":"text",
          "title":"Quiz HOT",
          "payload":"hot"
        },
        {
          "content_type":"text",
          "title":"Quiz mới nhất",
          "payload":"mới"
        },
        {
          "content_type":"text",
          "title":"Quiz khác",
          "payload":"khác"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a Structured Message (Generic Message type) using the Send API.
 *
 */
function sendGenericMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "rift",
            subtitle: "Next-generation virtual reality",
            item_url: "https://www.oculus.com/en-us/rift/",               
            image_url:"http://vi.topquiz.co/images/resources/thumb.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/rift/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for first bubble",
            }],
          }, {
            title: "touch",
            subtitle: "Your Hands, Now in VR",
            item_url: "https://www.oculus.com/en-us/touch/",               
            image_url:"http://vi.topquiz.co/images/resources/thumb.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/touch/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for second bubble",
            }]
          }]
        }
      }
    }
  };  

  callSendAPI(messageData);
}

/*
 * Send a receipt message using the Send API.
 *
 */
function sendReceiptMessage(recipientId) {
  // Generate a random receipt ID as the API requires a unique ID
  var receiptId = "order" + Math.floor(Math.random()*1000);

  var messageData = {
    recipient: {
      id: recipientId
    },
    message:{
      attachment: {
        type: "template",
        payload: {
          template_type: "receipt",
          recipient_name: "Peter Chang",
          order_number: receiptId,
          currency: "USD",
          payment_method: "Visa 1234",        
          timestamp: "1428444852", 
          elements: [{
            title: "Oculus Rift",
            subtitle: "Includes: headset, sensor, remote",
            quantity: 1,
            price: 599.00,
            currency: "USD",
            image_url: "http://vi.topquiz.co/images/resources/thumb.png"
          }, {
            title: "Samsung Gear VR",
            subtitle: "Frost White",
            quantity: 1,
            price: 99.99,
            currency: "USD",
            image_url:"http://vi.topquiz.co/images/resources/thumb.png"
          }],
          address: {
            street_1: "1 Hacker Way",
            street_2: "",
            city: "Menlo Park",
            postal_code: "94025",
            state: "CA",
            country: "US"
          },
          summary: {
            subtotal: 698.99,
            shipping_cost: 20.00,
            total_tax: 57.67,
            total_cost: 626.66
          },
          adjustments: [{
            name: "New Customer Discount",
            amount: -50
          }, {
            name: "$100 Off Coupon",
            amount: -100
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a message with Quick Reply buttons.
 *
 */
function sendQuickReply(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: "What's your favorite movie genre?",
      quick_replies: [
        {
          "content_type":"text",
          "title":"Action",
          "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_ACTION"
        },
        {
          "content_type":"text",
          "title":"Comedy",
          "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_COMEDY"
        },
        {
          "content_type":"text",
          "title":"Drama",
          "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_DRAMA"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a read receipt to indicate the message has been read
 *
 */
function sendReadReceipt(recipientId) {
  console.log("Sending a read receipt to mark message as seen");

  var messageData = {
    recipient: {
      id: recipientId
    },
    sender_action: "mark_seen"
  };

  callSendAPI(messageData);
}

/*
 * Call the Send API. The message data goes in the body. If successful, we'll 
 * get the message id in a response 
 *
 */
function callSendAPI(messageData) {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: "EAAECZAZCorA9wBAOk7NX5JKRXzLfceLEPL9sS5Nw7bvHB3WfB4CAuc3g677ZB8O3LY1kVsdtFFrIyig4f5f3iGFzuX0j1vrZC8lAiP9FDse4HdeZBTPjHrJ3PpuoJH6RqHRS3Bn5McbLuKRyPOMlyjfJOaDQ1YlSSNMrfNUpVxQZDZD"},
    method: 'POST',
    json: messageData

  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      if (messageId) {
        console.log("Successfully sent message with id %s to recipient %s", 
          messageId, recipientId);
      } else {
      console.log("Successfully called Send API for recipient %s", 
        recipientId);
      }
    } else {
      console.error("Failed calling Send API", response.statusCode, response.statusMessage, body.error);
    }
  });  
}

function sendMessage(senderId, message) {
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {
      access_token: "EAAECZAZCorA9wBAOk7NX5JKRXzLfceLEPL9sS5Nw7bvHB3WfB4CAuc3g677ZB8O3LY1kVsdtFFrIyig4f5f3iGFzuX0j1vrZC8lAiP9FDse4HdeZBTPjHrJ3PpuoJH6RqHRS3Bn5McbLuKRyPOMlyjfJOaDQ1YlSSNMrfNUpVxQZDZD",
    },
    method: 'POST',
    json: {
      recipient: {
        id: senderId
      },
      message: {
        text: message
      },
    }
  });
}

function startMessage(senderId, message) {
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {
      access_token: "EAAECZAZCorA9wBAOk7NX5JKRXzLfceLEPL9sS5Nw7bvHB3WfB4CAuc3g677ZB8O3LY1kVsdtFFrIyig4f5f3iGFzuX0j1vrZC8lAiP9FDse4HdeZBTPjHrJ3PpuoJH6RqHRS3Bn5McbLuKRyPOMlyjfJOaDQ1YlSSNMrfNUpVxQZDZD",
    },
    method: 'POST',
    get_started: {
      "payload":"BẮT ĐẦU"
    }
  });
}

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3001);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || process.env.IP || "127.0.0.1");

server.listen(app.get('port'), app.get('ip'), function() {
  console.log("Chat bot server listening at %s:%d ", app.get('ip'), app.get('port'));
});

module.exports = app;